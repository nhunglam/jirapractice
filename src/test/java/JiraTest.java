import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import pageobject.CreateIssuePage;
import pageobject.EditIssuePage;
import pageobject.IssuePage;
import pageobject.LoginPage;
import pageobject.SearchPage;


public class JiraTest {

    private WebDriver driver;

    private String browser = System.getProperty("browser", "FireFox");
    private String chromeDriverPath = System.getProperty("chromedriver");
    private String jiraPath = System.getProperty("jiraPath", "http://localhost:11990/jira");

    private void login(String userName, String password) {

        if(browser.equals("Chrome")) {
            System.setProperty("webdriver.chrome.driver", chromeDriverPath);
            driver = new ChromeDriver();
        } else if(browser.equals("Safari")) {
            driver = new SafariDriver();
        } else {
            driver = new FirefoxDriver();
        }

        driver.get(jiraPath + "/login.jsp");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(userName, password);
    }

    @Test
    public void testCreateIssue() {
        login("admin", "admin");

        CreateIssuePage createIssuePage = new CreateIssuePage(driver);
        createIssuePage.goToCreateIssuePage();

        String issueID = createIssuePage.createNewIssue("create new issue");

        driver.get(jiraPath + "/browse/" + issueID);

        IssuePage issuePage=new IssuePage(driver);
        issuePage.deleteIssue();

        driver.close();
    }

    @Test
    public void testUpdateIssue() {
        login("admin", "admin");

        CreateIssuePage createIssuePage = new CreateIssuePage(driver);
        createIssuePage.goToCreateIssuePage();

        String issueID = createIssuePage.createNewIssue("create new issue to test update feature");


        driver.get(jiraPath + "/browse/" + issueID);

        EditIssuePage editIssuePage=new EditIssuePage(driver);
        editIssuePage.updateIssue("Test update issue","Task","Critical","17/Mar/15","Testing Environment","Test update issue function");

        driver.get(jiraPath + "/browse/" + issueID);
        IssuePage issuePage=new IssuePage(driver);
        issuePage.verifyUpdateSuccess("Test update issue","Task","Critical","17/Mar/15","Testing Environment","Test update issue function");

        issuePage.deleteIssue();

        driver.close();
    }

    @Test
    public void testQuickSearch() {
        login("admin", "admin");

        SearchPage searchPage= new SearchPage(driver);

        CreateIssuePage createIssuePage = new CreateIssuePage(driver);
        createIssuePage.goToCreateIssuePage();

        String issueID = createIssuePage.createNewIssue("create new issue to test quick search");

        searchPage.quickSearch("create new issue to test quick search");

        searchPage.verifyResults(1);

        driver.get(jiraPath + "/browse/" + issueID);
        IssuePage issuePage=new IssuePage(driver);
        issuePage.deleteIssue();

        driver.close();
    }

    @Test
    public void testBasicSearch() {
        login("admin", "admin");

        SearchPage searchPage= new SearchPage(driver);

        searchPage.goToSearchPage();
        searchPage.goToBasicMode();
        searchPage.clickSearch();

        //Search all existing issues
        searchPage.verifyResults(6);

        //Search issue filter following Type
        searchPage.selectType("Bug");
        searchPage.verifyResults(1);

        searchPage.selectType("Task");
        searchPage.verifyResults(3);

        driver.close();
    }

    @Test
    public void testAdvancedSearch() {
        login("admin", "admin");

        SearchPage searchPage= new SearchPage(driver);

        searchPage.goToSearchPage();
        searchPage.goToAdvancedMode();
        searchPage.clickSearch();

        //Search all existing issues
        searchPage.verifyResults(6);

        //Search issue filter following Type
        searchPage.setJQL("type=Bug");
        searchPage.clickSearch();
        searchPage.verifyResults(1);

        searchPage.setJQL("type in (Task, Bug)");
        searchPage.clickSearch();
        searchPage.verifyResults(3);

        driver.close();
    }
}
