package pageobject;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class EditIssuePage {
    private final WebDriver driver;

    By editIssue=By.id("edit-issue");
    By summary=By.id("summary");
    By issuetype=By.cssSelector("#issuetype-single-select input");
    By priority =By.id("priority-field");
    By dueDate=By.id("duedate");
    By environment=By.id("environment");
    By description=By.id("description");
    By updateButton=By.id("edit-issue-submit");


    public EditIssuePage(WebDriver driver){
        this.driver=driver;
    }


    public void updateIssue(String summaryValue, String issueTypeValue, String priorityValue, String dueDateValue, String environmentValue, String descriptionValue){
        driver.findElement(editIssue).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(summary));

        driver.findElement(summary).sendKeys(summaryValue);

        driver.findElement(issuetype).clear();
        driver.findElement(issuetype).sendKeys(issueTypeValue);

        driver.findElement(priority).clear();
        driver.findElement(priority).sendKeys(priorityValue);


        driver.findElement(dueDate).sendKeys(dueDateValue);

        driver.findElement(environment).sendKeys(environmentValue);

        driver.findElement(description).sendKeys(descriptionValue);

        driver.findElement(updateButton).click();


    }


}
