package pageobject;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.String;


public class IssuePage {
    private final WebDriver driver;
    By moreDropdownList = By.className("dropdown-text");
    By delete = By.id("delete-issue");
    By deleteButton = By.id("delete-issue-submit");
    By summary = By.id("summary-val");
    By issuetype = By.id("type-val");
    By priority = By.id("priority-val");
    By dueDate = By.cssSelector("#due-date time");
    By environment = By.cssSelector("#environment-val p");
    By description = By.cssSelector(".user-content-block p");


    public IssuePage(WebDriver driver) {
        this.driver = driver;
    }

    public void deleteIssue() {
        driver.findElement(moreDropdownList).click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(delete));

        driver.findElement(delete).click();

        WebDriverWait wait1 = new WebDriverWait(driver, 10);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(deleteButton));

        driver.findElement(deleteButton).click();

    }

    public void verifyUpdateSuccess(String summaryValue, String issueTypeValue, String priorityValue, String dueDateValue, String environmentValue, String descriptionValue) {
        String summaryValueText = driver.findElement(summary).getText();
        Assert.assertTrue(summaryValue, summaryValueText.equals(summaryValue));

        String issueTypeValueText = driver.findElement(issuetype).getText();
        Assert.assertTrue(issueTypeValueText.equals(issueTypeValue));

        String priorityValueText = driver.findElement(priority).getText();
        Assert.assertTrue(priorityValueText.equals(priorityValue));

        String dueDateValueText = driver.findElement(dueDate).getText();
        Assert.assertTrue(dueDateValueText.equals(dueDateValue));

        String environmentValueText = driver.findElement(environment).getText();
        Assert.assertTrue(environmentValueText.equals(environmentValue));

        String descriptionValueText = driver.findElement(description).getText();
        Assert.assertTrue(descriptionValueText.equals(descriptionValue));

    }
}
