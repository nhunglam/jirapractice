package pageobject;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Keys;


public class SearchPage {
    private final WebDriver driver;
    By issue = By.id("find_link");
    By searchForIssue = By.id("issues_new_search_link_lnk");
    By issueList = By.cssSelector(".issue-list li");
    By numberOfIssue = By.className("showing");
    By quickSearch = By.id("quickSearchInput");
    By modeSwicher = By.cssSelector(".switcher-item.active");
    By searchButton=By.cssSelector(".aui-icon.aui-icon-small.aui-iconfont-search");
    By typeButton=By.cssSelector("button[data-id='issuetype']");
    By searchTypeInput=By.id("searcher-type-input");
    By searchAdvancedInput=By.id("advanced-search");

    public SearchPage(WebDriver driver) {
        this.driver = driver;
    }

    public void quickSearch(String textSearch) {
        WebElement quickSearchElement = driver.findElement(quickSearch);

        quickSearchElement.clear();
        quickSearchElement.sendKeys(textSearch);
        quickSearchElement.sendKeys(Keys.ENTER);

    }

    public void goToBasicMode() {
        WebElement modeSwitchElement = driver.findElement(modeSwicher);
        if (modeSwitchElement.getText().equals("Basic")){
            modeSwitchElement.click();
        }
    }

    public void goToAdvancedMode() {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(modeSwicher));

        WebElement modeSwitchElement = driver.findElement(modeSwicher);
        if (modeSwitchElement.getText().equals("Advanced")) {
            modeSwitchElement.click();
        }
    }

    public void goToSearchPage(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(issue));
        driver.findElement(issue).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(searchForIssue));
        driver.findElement(searchForIssue).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(modeSwicher));
    }

    public void clickSearch(){
        driver.findElement(searchButton).click();
    }

    public void verifyResults(int numberIssue){

        WebDriverWait wait1 = new WebDriverWait(driver, 10);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(numberOfIssue));

        String numberOfIssueValue = driver.findElement(numberOfIssue).getText();
        String number = (numberOfIssueValue).split(" ")[2];
        Assert.assertTrue(number.equals(String.valueOf(numberIssue)));

        int countList = driver.findElements(issueList).size();
        Assert.assertTrue(countList == numberIssue);

    }

    public void selectType(String type){
        WebElement typeButtonElement =  driver.findElement(typeButton);
        typeButtonElement.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchTypeInput));
        WebElement searchTypeInputValue= driver.findElement(searchTypeInput);
        searchTypeInputValue.sendKeys(type);
        searchTypeInputValue.sendKeys(Keys.ENTER);
        typeButtonElement.click();

    }

    public void setJQL(String value){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(searchAdvancedInput));
        WebElement searchInput=driver.findElement(searchAdvancedInput);
        searchInput.clear();
        searchInput.sendKeys(value);
    }
}