package pageobject;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateIssuePage {
    private final WebDriver driver;
    By createIssue = By.id("create_link");
    By summary = By.id("summary");
    By createIssueButton = By.id("create-issue-submit");
    By createSucessfulMessage = By.cssSelector(".aui-message.success.closeable");

    public CreateIssuePage(WebDriver driver) {
        this.driver = driver;
    }

    public void goToCreateIssuePage() {
        driver.findElement(createIssue).click();

    }

    public String createNewIssue(String summaryValue) {

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(summary));
        driver.findElement(summary).sendKeys(summaryValue);
        driver.findElement(createIssueButton).click();

        WebDriverWait wait1 = new WebDriverWait(driver, 10);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(createSucessfulMessage));

        String message = driver.findElement(createSucessfulMessage).getText();

        Assert.assertTrue(message.contains("has been successfully created"));

        return message.split(" ")[1];

    }


}
