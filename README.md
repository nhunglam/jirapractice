## EVN setup ##

1. Install new JIRA instance
2. Create a Demo Project of Jira (6 existing issues)
3. Setup Maven

## How to run ##

1. Checkout lira practice code: repository link:https://bitbucket.org/nhunglam/jirapractice
2. Go to code folder
3. Run automation test: The automation test is supported to run 3 browsers: Firefox, Chrome and Safari.

       3.1 Firefox:
        - Run command: mvn clean test -DjiraPath={jira-server} 
 
       3.2 Chrome:
       - Download ChromeDriver: https://sites.google.com/a/chromium.org/chromedriver/
       - Run command: mvn clean test -DjiraPath={jira-server} -Dbrowser=Chrome -Dchromedriver={path-chrome-driver}
 
      3.3 Safari:
      - Follow these step to install webdriver for safari http://watirwebdriver.com/safari/
      - Run command: mvn clean test -DjiraPath={lira-server} -Dbrowser=Safari

## Testing description: ##

I. New issue can be created: 
   For this test I just create automation test for a happy case: create issue successfully (there are some cases as create issue unsuccessfully or validation cases I did not implement ). The test name is testCreateIssue. 

   1. Log in to the JIRA system, navigate to Create Issue page.
   2. Create a issue with entering value for Summary field and keeping default value for other fields. 
   3. Verify the successful message should be displayed
   4. Verify the issue should be existed in the system.
   5. After completing the test I delete the issue.

II. Existing issue can be updated: 
   For this test I just create automation test for a happy case:update issue successfully (there are some cases as update issue unsuccessfully or validation cases I did not implement )  I create a test with name testUpdateIssue. In this test, I implement following steps:
   1. Log in to the JIRA system, navigate to Create Issue page.
   2. Create a new issue to test.
   3. Open the issue then update following fields: Summary, Issue Type, Priority, Due Date, Description, Environment.
   4. Open the issue again.
   5. Verify that values of the updated fields should be changed.
   6. After completing the test I delete the issue.

III. Existing issues can be found be JIRA’search: 
     There are many test cases for this feature such as search with wrong parameters, combination filter for search advance and search basic etc... I just create 3 automation tests for “Quick Search”, “Basic Search” , and “Advanced Search”: For Basic Search and Advanced Search I just implement searching following Issue type.

   1. Test Quick Search: Test name is testQuickSearch with steps following:

     1.1 Login to the JIRA system, Navigate to Ceate New Issue page
   
     1.2 Create a new issue with entering a Summary value.
   
     1.3. At Quick Search Textbox on the right hand site, enter the Summary value then press ENTER key.
   
     1.4 Verify Search Results from page showing number should be 1 issue found.
  
     1.5 Verify Search results from issues list should have 1 issue in the list.

     1.6 After completing the test I delete the issue.

   2. Test Basic Search: Test name is testBasicSearch. In this test, I search issues with Type=“Bug” and Type=“Bug and Task” with steps following:

      2.1 Login to the JIRA system.
   
      2.2 Click on Issue list.
   
      2.3 Select “Search for Issues” option.
   
      2.4 Click Search button.
   
      2.5 Verify that all issues (6 issues in the system) should be displayed.
   
     2.6 Navigate to search Basic mode.
   
     2.7 Select Type=“Bug”.
   
     2.8 Click Search button.
  
     2.9 Verify that there is 1 matched issue is displayed.
   
    2.10 Select Type=“Task”.
 
    2.11 Click Search button.
  
    2.12 Verify that there are 3 matched issues are displayed.


  3. Test Advanced Search: Test name is testAdvanceSearch. Same as test Basic search, I search issues with entering JQL as: Type=“Bug” and Type=“Bug and Task” with steps following:

     3.1 Login to the JIRA system.
  
     3.2 Click on Issue list.
  
     3.3 Select “Search for Issues” option.
  
     3.4 Click Search button.
   
     3.5 Verify that all issues (6 issues in the system) should be displayed.
  
     3.6 Navigate to search Advanced mode.
  
     3.7 Enter JQL: Type=“Bug”.
  
     3.8 Click Search button.
  
     3.9 Verify that there is 1 matched issue is displayed.
  
     3.10 Enter JQL: type in (Bug, Task).
  
     3.11 Click Search button.
  
     3.12 Verify that there are 3 issue is displayed.


## Supporter ##

- Guide me to setup environments: Khoa Pham.